
var app = angular.module("app", []);

app.config(function($httpProvider) {
	$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
});
	
app.controller("navigation", function($http, $location) {
	var self = this;

	self.logout = function() {
		$http.post('/logout', {})
		.success(function() {
			window.location.reload(true);
		})
		.error(function(data) {
			window.location.reload(true);
		});
	};
});

// Controller for tasks listing
app.controller("main", function ($http, $scope) {
	var self = this;
	self.taskId = -1;
	$scope.error = false;
	
	self.getTasks = function () {
		$http.get("/task")
		.success(function(data) {
			$scope.error = false;
			$scope.tasks = data;
		})
		.error(function() { $scope.error = true; });
	};
	
	$scope.rememberId = function (id) {
		self.taskId = id;
	};
	
	$scope.remove = function () {
		$http.delete("/task/"+self.taskId)
			.success(function(data) {
				$scope.error = false;
				self.getTasks();
				$("#myModal").modal("hide");
			})
			.error(function(data) { 
				$scope.error = true;
				$("#myModal").modal("hide");
			});
	};
	
	self.getTasks();
});

// Controller for adding new task
app.controller("addtask", function ($http, $scope) {
	var self = this;
	var taskId = $('#taskId').val();
	var groupId = $('#groupId').val();
	self.error = false;
	self.emptyFields = false;
	self.favouriteClass = "btn-warning";
	$scope.classes = {
		taskNameField: "",
		groupNameField: ""
	};
	self.submitSuccess = false;

	self.validate = function () {
		self.emptyFields = false;
		$scope.classes.taskNameField = $scope.classes.groupNameField = "";
		if (!$scope.taskName || $scope.taskName<1) {
			self.emptyFields = true;
			$scope.classes.taskNameField = "has-warning";
			return false;
		}
		if ((!$scope.groupName || $scope.groupName<1) && !groupId>0) {
			$scope.classes.groupNameField = "has-warning";
			self.emptyFields = true;
			return false;
		}
		return true;
	};

	self.clearScope = function () {
		$scope.taskName = null;
		$scope.taskDescription = null;
		$scope.taskText = null;
		$scope.favourite = null;
		$scope.groupName = null;
	};
	
	self.getGroups = function () {
		$http.get("/grouped", {})
		.success(function(data) {
			for (var property in data) {
				if (data.hasOwnProperty(property) && data[property].id == groupId) {
					
				}
			}
			$scope.groups = data;
		})
		.error(function() {});
	};
	
	
	self.submit = function() {
		if (!self.validate()) {
			return ;
		}
		
		var tmpArr = [];
		var formGroups = $('#form-list-container > .form-group');
		for (var i=0, length=formGroups.length; i<length; i++) {
			var element = $(formGroups[i]);
			
			var subtaskName = element.find('.subtask-name').val();
			var isRequired = element.find('.is-required').val();
			var id = element.find('.listId').val();
			tmpArr.push({ 'text' : subtaskName, 'isCompleted' : (isRequired==1?false:true) , 'id' : id});
		}

		var Indata = {
			'id' : taskId,
			'name': $scope.taskName,
			'description': $scope.taskDescription,
			'text':	$scope.taskText,
			'isFavoured': ($scope.favourite==1),
			'list':	tmpArr
		};
		
		var grouped = {
			id: ($scope.groupName ? $scope.groupName : groupId),
			colour: 'BLUE',
			tasks: [Indata]
		};
		
		if (groupId) {
			$http.put("/task", grouped)
			.success(function(data) {
				self.error = false;
				self.submitSuccess = true;
				$('#adding-task-form').trigger("reset");
				$('#form-list-container').empty();
				self.clearScope();
			})
			.error(function() {
				self.error = true;
				self.submitSuccess = false;
			});
		} else {
			$http.post("/task", grouped)
			.success(function(data) {
				self.error = false;
				self.submitSuccess = true;
				$('#adding-task-form').trigger("reset");
				$('#form-list-container').empty();
				self.clearScope();
			})
			.error(function() {
				self.error = true;
				self.submitSuccess = false;
			});
		}
	
	};
	
	self.inverseFavourite = function() {
		if (self.favouriteClass.indexOf("btn-warning")>-1) {
			self.favouriteClass = "btn-default";
			$scope.favourite = 0;
		} else {
			self.favouriteClass = "btn-warning";
			$scope.favourite = 1;
		}
	};
	
	
	self.addingSucceded = function () {
		self.submitSuccess = true;
		$scope.groupName = null;
		$scope.groupColour = null;
	};
	
	self.focused = function () {
		self.submitSuccess = false;
	};
	
	self.getGroups();
	$scope.taskId = taskId;
	$scope.taskName = $('#taskName').val();
	$scope.taskDescription = $('#taskDescription').val();
	$scope.taskText = $('#taskText').val();
	$scope.groupId = groupId;
	if ($('#taskFavourite').val()) {
		self.inverseFavourite();
	}
});

// Controller for adding groups 
app.controller("addgroup", function ($http, $scope, $location) {
	var self = this;
	var groupColour = $('#groupColour').val();
	var groupName = $('#groupName').val();
	var groupId = $('#groupId').val();
	self.error = false;
	self.emptyFields = false;
	self.submitSuccess = false;
	self.groupNameClass = "";
	self.colourPickerClass = "";
	

	self.submit = function() {
		var Indata = {'name': $scope.groupName, 'colour': self.colourMapper($scope.groupColour) };
		var groupId = $('#groupId').val();
		
		if (!Indata.name || Indata.name.length<1) {
			self.emptyFields = true;
			self.groupNameClass = "has-warning";
			return;
		}
		if (!Indata.colour || Indata.colour.length<1) {
			self.emptyFields = true;
			self.colourPickerClass = "has-warning";
			return;
		}
		
		self.emptyFields = false;
		self.colourPickerClass = self.groupNameClass = ""
		
		if (groupId) {
			Indata.id = groupId;
			$http.put("/grouped", Indata)
			.success(function(data) {
				self.error = false;
				self.addingSucceded("modified");
			})
			.error(function(data) {
				self.error = true;
				self.submitSuccess = false;
			});
		} else {
			$http.post("/grouped", Indata)
			.success(function(data) {
				self.error = false;
				self.addingSucceded("added!");
			})
			.error(function(data) {
				self.error = true;
				self.submitSuccess = false;
			});
		}
	};
	
	self.addingSucceded = function (message) {
		$scope.infoMessage = message;
		self.submitSuccess = true;
		$scope.groupName = null;
		$scope.groupColour = null;
	};
	
	self.focused = function () {
		self.submitSuccess = false;
	};
	
	self.colourMapper = function (colour) {

		var mapper = {
			"#7bd148" : "GREEN",
			"#a4bdfc" : "BLUE",
			"#46d6db" : "TEAL_BLUE",
			"#fbd75b" : "YELLOW",
			"#ffb878" : "ORANGE",
			"#ff887c" : "RED",
			"#dbadff" : "PURPLE",
			"#f691b2" : "PINK"
		};

		return mapper[colour];
	};
	
	self.reverseColourMapper = function (colour) {

		var mapper = {
			"GREEN" 	: "#7bd148",
			"BLUE"		: "#a4bdfc",
			"TEAL_BLUE" : "#46d6db",
			"YELLOW" 	: "#fbd75b",
			"ORANGE" 	: "#ffb878",
			"RED" 		: "#ff887c",
			"PURPLE" 	: "#dbadff",
			"PINK"		: "#f691b2"
		};

		return mapper[colour];
	};
	
	var colour = self.reverseColourMapper(groupColour);
	if (colour) {
		$('select[name="colorpicker"]').simplecolorpicker('selectColor', colour);
	}
	$scope.groupName = groupName;
	$scope.groupId = groupId;
});

app.filter('reverseColour', function() {
    return function(x) {
    	var mapper = {
			"GREEN" 	: "#7bd148",
			"BLUE"		: "#a4bdfc",
			"TEAL_BLUE" : "#46d6db",
			"YELLOW" 	: "#fbd75b",
			"ORANGE" 	: "#ffb878",
			"RED" 		: "#ff887c",
			"PURPLE" 	: "#dbadff",
			"PINK"		: "#f691b2"
		};
    	return mapper[x];
    };
});

// Controller for group listing
app.controller("listgroups", function ($http, $scope) {
	var self = this;
	self.groupId = -1;
	$scope.error = false;
	
	self.reverseColourMapper = function (colour) {
		return mapper[colour];
	};
	
	self.getGroups = function () {
		$http.get("/grouped", {})
		.success(function(data) {
			$scope.groups = data;
			$scope.error = false;
		}).error(function() { $scope.error = true; });
	};
	
	$scope.rememberId = function (id) {
		self.groupId = id;
	};
	
	$scope.remove = function () {
		$http.delete("/grouped/"+self.groupId)
			.success(function(data) {
				$scope.error = false;
				self.getGroups();
				$("#myModal").modal("hide");
			})
			.error(function(data) { 
				$scope.error = true;
				$("#myModal").modal("hide");
			});
	};
	
	self.getGroups();
});