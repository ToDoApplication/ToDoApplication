package com.springsecurity;

import java.net.URI;
import java.security.Principal;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.springsecurity.dao.Account;
import com.springsecurity.dao.Grouped;
import com.springsecurity.model.AccountRepository;
import com.springsecurity.model.GroupedRepository;

/**
 * <p>
 * Klasa kontrolera RESTowego dla operacji na grupach.
 * Klasa automaytcznie zarejestrowana jako Bean dzięki adnotacji kontrolera. Dodaje główną ścieżkę do
 * mapowania, a rozróżnia operacje zgodnie z przyjętą koncepcją REST, rozróżniając operacje nagłówkami.
 * </p>
 * 
 * @author karolczukm
 */
@RestController
@RequestMapping(GroupedRestController.REQUEST_NAME)
public class GroupedRestController {

	
	static final String REQUEST_NAME = "/grouped";
	
	private final GroupedRepository groupedRepository;
	
	private final AccountRepository accountRepository;
	
	/**
	 * <p>
	 * Kontroler dostarczający Beany {@link GroupedRepository} oraz {@link AccountRepository}, tak więc 
	 * wstrzykujący zależności dla wykorzystywanych repozytoriów.
	 * </p>
	 * 
	 * @param groupedRepository Repozytorium dla group
	 * @param accountRepository Repozytorium dla kont
	 */
	@Autowired
	GroupedRestController(GroupedRepository groupedRepository, AccountRepository accountRepository) {
		this.groupedRepository = groupedRepository;
		this.accountRepository = accountRepository;
	}
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za pobieranie wszystkich group dostępnych dla zautoryzowanego użytkownika.
	 * Jeżeli nie potrafi znaleźć powiązanego konta zalogowanego użytkownika to zwraca wartość pustą.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @return Wszystkie grupy {@link Grouped} w kolekcji zbioru {@link Set}
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Set<Grouped> showGroupeds(Principal principal) {
		Optional<Account> account = accountRepository.findByUsername(principal.getName());
		if (!account.isPresent()) {
			return null;
		}
		return account.get().getGroupeds();
	}
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za pobieranie konkretnej grupy po dodaniu do ścieżki parametru identyfikatora groupy.
	 * Odpowiada ścieżce z parametrem całkowitoliczbowym oraz żądaniu typu GET.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param groupedId Identyfikator szukanej grupy {@link Long}
	 * @return {@link Grouped} Instancję grupy lub wartość pustą, gdy nie ma użytkownika w bazie lub nie ma grupy.
	 */
	@RequestMapping(method = RequestMethod.GET, value="/{groupedId}")
	public Grouped showGrouped(Principal principal, @PathVariable Long groupedId) {
		Optional<Account> account = accountRepository.findByUsername(principal.getName());
		if (!account.isPresent()) {
			return null;
		}
		return groupedRepository.findOne(groupedId);
	}
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za zapis grupy na podstawie ciała żądania zawartego w żądaniu typu POST.
	 * Jeżeli nie może przetworzyć ciała żądania to zwraca status 400 z odpowiedzią o źle sformatownym 
	 * ciele JSON żądania.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param grouped {@link Grouped} Ciało żądania przetworzona przez Jackson do konkretnej instancji.
	 * @return Jeden z dwóch statusów, że udało się dodać groupę lub brak zawartości.
	 */
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> addGrouped(Principal principal, @RequestBody Grouped grouped) {
		
		return this.accountRepository
			.findByUsername(principal.getName())
			.map(account -> {
				// TODO: Sprawdzenie ręczne czy dla podanego użytkownika istnieje już grupa, gdyż w encji gryzie się z tymi samymi nazwami róznym kontom
				Grouped newGroup = new Grouped(account, grouped.getName(), grouped.getColour());
				groupedRepository.save(newGroup);
				
				URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path("/{groupedId}")
					.buildAndExpand(newGroup.getId()).toUri();
				
				return ResponseEntity.created(location).build();
			})
			.orElse(ResponseEntity.noContent().build());
	}
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za aktualizację groupy, na podstawie przesłanej instancji grupy z ciała żądania.
	 * Jeżeli nie może przetworzyć ciała żądania to zwraca status 400 z odpowiedzią o źle sformatownym 
	 * ciele JSON żądania.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param grouped {@link Grouped} Ciało żądania przetworzona przez Jackson do konkretnej instancji.
	 * @return Jeden z dwóch statusów, że udało się aktualizować groupę lub brak zawartości.
	 */
	@RequestMapping(method = RequestMethod.PUT)
	ResponseEntity<?> updateGrouped(Principal principal, @RequestBody Grouped grouped) {
		
		return this.accountRepository
			.findByUsername(principal.getName())
			.map(account -> {
				Grouped updateGrouped = groupedRepository.getOne(grouped.getId());
				if (updateGrouped!=null) {
					updateGrouped.setName(grouped.getName());
					updateGrouped.setColour(grouped.getColour());
					groupedRepository.save(updateGrouped);
					
					URI location = ServletUriComponentsBuilder
						.fromCurrentRequest().path("/{groupedId}")
						.buildAndExpand(updateGrouped.getId()).toUri();
					
					return ResponseEntity.created(location).build();
				}
				return ResponseEntity.noContent().build();
			})
			.orElse(ResponseEntity.noContent().build());
	}
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za kasowanie rekordu z encji grup.
	 * Usuwa rekord na podstawie parametru podanym w ścieżce, który odpowiadać ma 
	 * wartości typu całowitego.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param groupedId Identyfikator szukanej grupy {@link Long}
	 * @return Jeden z dwóch statusów, że udało się usunąć groupę lub brak zawartości.
	 */
	@RequestMapping(method = RequestMethod.DELETE, value="/{groupedId}")
	ResponseEntity<?> deleteGrouped(Principal principal, @PathVariable Long groupedId) {
		
		return this.accountRepository
			.findByUsername(principal.getName())
			.map(account -> {
				groupedRepository.delete(groupedId);
				
				URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path(GroupedRestController.REQUEST_NAME)
					.buildAndExpand().toUri();
				
				return ResponseEntity.created(location).build();
			})
			.orElse(ResponseEntity.noContent().build());
	}
}
