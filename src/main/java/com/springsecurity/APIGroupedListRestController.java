package com.springsecurity;

import com.springsecurity.dao.Grouped;
import com.springsecurity.model.GroupedRepository;
import com.utils.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Marcin Jucha on 08.06.2017.
 */
@RestController
@RequestMapping(value = "/api/grouped/*")
public class APIGroupedListRestController {

    private final GroupedRepository groupedRepository;

    @Autowired
    public APIGroupedListRestController(GroupedRepository groupedRepository) {
        this.groupedRepository = groupedRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Grouped group(Long id, HttpServletRequest req, HttpServletResponse res) {
        String username = JWTUtils.getAuthentication(req);

        if (username != null) {
            return groupedRepository.findOne(id);
        }
        res.setStatus(401);
        return null;
    }


}
