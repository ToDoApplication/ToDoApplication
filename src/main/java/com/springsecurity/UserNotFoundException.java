package com.springsecurity;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Uszczegółowanie wyjątku czasu wykonania, które sygnalizuje próbę pobrania szczegółów
 * użytkownika niezautoryzowanego.
 * 
 * @author karolczukm
 */
@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

	public UserNotFoundException(String userId) {
		super("could not find user '" + userId + "'.");
	}
}
