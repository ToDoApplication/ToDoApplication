package com.springsecurity.dao;

/**
 * <p>
 * Klasa wyliczeniowa reprezentująca kolory, które można przyporządkować grupom.
 * Wybrane na podstawie palety dostępnej w iOSie.
 * </p>
 * 
 * @author karolczukm
 * @see Grouped
 */
public enum Colour {
	RED,
	BLUE,
	GREEN,
	ORANGE,
	YELLOW,
	TEAL_BLUE,
	PURPLE,
	PINK
}
