package com.springsecurity.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.springsecurity.APIAccountRestController;
import com.springsecurity.CustomOAuth2ClientAuthenticationProcessingFilter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 * Klasa reprezentująca rekord zalogowanego użytkownika do przechowywania w bazie danych.
 * Adnotacja klasy dodaje do ścieżki przeszukiwania dla @SpringBootApplication.
 * </p>
 * <p>
 * Klasa utworzona w celu zapewnienia sposobu wymiany danych o zalogowanych użytkownikach między klientami
 * webowymi oraz mobilnymi. Instancja tworzona jest w momencie poprawnej autoryzacji przy którymkolwiek z dostępnych
 * dostawców. {@link CustomOAuth2ClientAuthenticationProcessingFilter} dla sposobu zapisu nowych kont lub {@link APIAccountRestController}
 * dla sposobu zapisu po autoryzacji mobilnej.
 * </p>
 * @author karolczukm
 */
@Entity
public class Account {

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
	private Set<Grouped> groupeds = new HashSet<>();
	
	@Id
	@GeneratedValue
	private Long id;
	
	@JsonIgnore
	public String password;
	public String username;
	private String email;
	
	public Account(String name, String password, String email) {
		this.username = name;
		this.password = password;
		this.email = email;
	}

	public Account() {}

	public Account(String name, String password) {

	}


    public Long getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<Grouped> getGroupeds() {
		return groupeds;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
