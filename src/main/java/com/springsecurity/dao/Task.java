package com.springsecurity.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 * Klasa reprezentująca rekord zadania w bazie danych.
 * Składa się z elementów kroków wykonania zadania, a należy do utworzonych grup.
 * </p>
 * 
 * <p>
 * Adnotacja klasy dodaje do ścieżki przeszukiwania dla @SpringBootApplication.
 * Konstruktor domyślny musi mieć przynajmniej dostęp domyślny.
 * </p>
 * @author karolczukm
 */
@Entity
public class Task {

	@JsonIgnore
	@ManyToOne
	private Grouped grouped;

	@OneToMany(mappedBy = "task", cascade = CascadeType.ALL)
	private Set<ToDoList> list = new HashSet<>();
	
	@Id
	@GeneratedValue
	private Long id;
	
	/** Nie dodajemy unikalności, gdyż różne grupy mogą mieć zadania o takiej samej nazwie */
	@Column(nullable = false)
	private String name;
	private String description;
	private String text;
	
	private boolean isFavored;
	
	
	public Task() {}
	
	public Task(Grouped grouped, String name, String description, String text, boolean isFavored) {
		this.name = name;
		this.description = description;
		this.grouped = grouped;
		this.text = text;
		this.isFavored = isFavored;
	}

	
	public Grouped getGrouped() {
		return grouped;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getText() {
		return text;
	}

	public String getDescription() {
		return description;
	}

	public boolean getIsFavored() {
		return isFavored;
	}

	public Set<ToDoList> getList() {
		return list;
	}

	public void setList(Set<ToDoList> list) {
		this.list = list;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setFavored(boolean favored) {
		isFavored = favored;
	}
}
