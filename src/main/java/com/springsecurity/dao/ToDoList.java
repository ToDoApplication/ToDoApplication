package com.springsecurity.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * <p>
 * Klasa reprezentująca rekordy listy kroków dla zadania.
 * Nie posiada własnego dostarczyciela danych w postaci kontrolera, gdyż istnienie samodzielne 
 * rekordu w widoku nie wydaje się sensowne.
 * </p>
 * 
 * @author jucham
 * @author karolczukm
 */
@Entity
public class ToDoList {

	@JsonIgnore
	@ManyToOne
	private Task task;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String text;
	
	@Column(nullable = false)
	private boolean isCompleted;
	
	ToDoList() {}
	
	public ToDoList(Task task, String text, boolean isCompleted) {
		this.task = task;
		this.text = text;
		this.isCompleted = isCompleted;
	}

	public ToDoList(String text, boolean isCompleted) {
		this.text = text;
		this.isCompleted = isCompleted;
	}

	public Task getTask() {
		return task;
	}

	public Long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public boolean getIsCompleted() {
		return isCompleted;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setCompleted(boolean completed) {
		isCompleted = completed;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
