package com.springsecurity.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <p>
 * Klasa reprezentująca rekord groupy do przechowywania w bazie danych.
 * Adnotacja klasy dodaje do ścieżki przeszukiwania dla @SpringBootApplication.
 * </p>
 * @author karolczukm
 */
@Entity
public class Grouped {

	@OneToMany(mappedBy = "grouped", cascade = CascadeType.ALL)
	private List<Task> tasks = new ArrayList<>();

	@JsonIgnore
	@ManyToOne
	private Account account;
	
	@Id
	@GeneratedValue
	private Long id;
	
	/** Nie dodajemy unikalności, gdyż różni użytkownicy mogą mieć taką samą nazwę grupy, walidacja przeprowadzana w kontrolerze */
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Colour colour;
	
	public Grouped() {}

	public Grouped(Account account, String name, Colour colour) {
		this.account = account;
		this.name = name;
		this.colour = colour;
	}

	public Grouped(Account account, String name) {
		this.account = account;
		this.name = name;
		this.colour = Colour.BLUE;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}

	public Account getAccount() {
		return account;
	}

	public Colour getColour() {
		return colour;
	}

	public String getDescription() {
		return null;
	}
}
