package com.springsecurity.model;

import com.springsecurity.dao.Grouped;
import com.springsecurity.dao.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Interfejs danych wystawiający dodatkowe do domyślnych sposoby manipulacji danymi dla rekordów 
 * zadań w istniejących już grupach.
 * 
 * @author karolczukm
 */
public interface TaskRepository extends JpaRepository<Task, Long> {
	List<Task> findByGrouped(Grouped grouped);
}
