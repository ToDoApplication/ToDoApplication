package com.springsecurity.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springsecurity.dao.Task;
import com.springsecurity.dao.ToDoList;

/**
 * Created by Marcin Jucha on 06.06.2017.
 */
public interface ToDoListRepository extends JpaRepository<ToDoList, Long> {
	List<ToDoList> findAllByTask(Task task);
}
