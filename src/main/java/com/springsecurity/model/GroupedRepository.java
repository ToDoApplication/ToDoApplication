package com.springsecurity.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springsecurity.dao.Account;
import com.springsecurity.dao.Grouped;

/**
 * Interfejs danych wystawiający dodatkowe do domyślnych sposoby manipulacji danymi dla rekordów 
 * grup zadań.
 * 
 * @author karolczukm
 */
public interface GroupedRepository extends JpaRepository<Grouped, Long>{
	List<Grouped> findByAccount(Account account);
	Grouped findByName(String name);
}
