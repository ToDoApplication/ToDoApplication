package com.springsecurity.model;

import com.springsecurity.dao.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Interfejs danych wystawiający dodatkowe do domyślnych sposoby manipulacji danymi dla rekordów 
 * kont użytkowników, którym udało się zautoryzować.
 * 
 * @author karolczukm
 */
public interface AccountRepository extends JpaRepository<Account, Long> {
	Optional<Account> findByUsername(String username);
}
