package com.springsecurity;

import com.request.model.GroupedModel;
import com.springsecurity.dao.Account;
import com.springsecurity.dao.Grouped;
import com.springsecurity.model.AccountRepository;
import com.springsecurity.model.GroupedRepository;
import com.utils.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Optional;
import java.util.Set;

/**
 * Created by Marcin Jucha on 05.06.2017.
 */
@RestController
@RequestMapping(value = "/api/grouped")
public class APIGroupedRestController {

    private final AccountRepository accountRepository;

    private final GroupedRepository groupedRepository;

    @Autowired
    public APIGroupedRestController(AccountRepository accountRepository, GroupedRepository groupedRepository) {
        this.accountRepository = accountRepository;
        this.groupedRepository = groupedRepository;
    }

    @RequestMapping()
    public Set<Grouped> list(HttpServletRequest req, HttpServletResponse res) {
        String username = JWTUtils.getAuthentication(req);

        return accountRepository.findByUsername(username).get().getGroupeds();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Grouped add(@RequestBody @Valid GroupedModel model, HttpServletRequest req, HttpServletResponse res) {
        String username = JWTUtils.getAuthentication(req);

        if (username != null) {
            Optional<Account> account = accountRepository.findByUsername(username);

            Grouped grouped = new Grouped(account.get(), model.getName(), model.getColour());
            groupedRepository.save(grouped);
            return grouped;
        }
        res.setStatus(401);
        return null;
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void update(@RequestBody @Valid GroupedModel model, HttpServletRequest req, HttpServletResponse res) {
        String username = JWTUtils.getAuthentication(req);

        if (username != null) {
            Grouped grouped = groupedRepository.findOne(model.getId());
            grouped.setName(model.getName());
            grouped.setColour(model.getColour());
            groupedRepository.save(grouped);
            return;
        }
        res.setStatus(401);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody @Valid GroupedModel model, HttpServletRequest req, HttpServletResponse res) {
        String username = JWTUtils.getAuthentication(req);

        if (username != null) {
            groupedRepository.delete(model.getId());
            return;
        }
        res.setStatus(401);
    }
}