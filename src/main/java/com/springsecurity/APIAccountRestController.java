package com.springsecurity;

import com.request.model.LoginModel;
import com.springsecurity.dao.Account;
import com.springsecurity.dao.Grouped;
import com.springsecurity.model.AccountRepository;
import com.springsecurity.model.GroupedRepository;
import com.utils.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marcin Jucha on 05.06.2017.
 */
@RestController
@RequestMapping("/api/login")
public class APIAccountRestController {
    private final AccountRepository accountRepository;
    private final GroupedRepository groupedRepository;

    @Autowired
    public APIAccountRestController(AccountRepository accountRepository, GroupedRepository groupedRepository) {
        this.accountRepository = accountRepository;
        this.groupedRepository = groupedRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    public void login(@RequestBody LoginModel model, HttpServletResponse response) {

        String GET_URL = "https://graph.facebook.com/app?access_token=" + model.getToken();
        RestTemplate template = new RestTemplate();
        try {
            template.getForObject(GET_URL, String.class);

            JWTUtils.addAuthentication(response, model.getUsername());

            Optional<Account> account = accountRepository.findByUsername(model.getUsername());
            if (!account.isPresent()) {
                Account newUser = new Account();
                newUser.setUsername(model.getUsername());
                accountRepository.save(newUser);

                Grouped grouped = new Grouped(newUser, "Inbox");
                groupedRepository.save(grouped);
            }
        } catch (Exception exp) {
            response.setStatus(401);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Account> list() {
        return accountRepository.findAll();
    }

}