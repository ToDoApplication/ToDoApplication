package com.springsecurity;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;

import com.springsecurity.dao.Account;
import com.springsecurity.model.AccountRepository;

public class CustomOAuth2ClientAuthenticationProcessingFilter extends OAuth2ClientAuthenticationProcessingFilter {

	private final AccountRepository accountRepository;
	
	
	@Autowired
	public CustomOAuth2ClientAuthenticationProcessingFilter(String defaultFilterProcessesUrl, AccountRepository accountRepository) {
		super(defaultFilterProcessesUrl);
		this.accountRepository = accountRepository;
	}

	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		String username = authResult.getPrincipal().toString();
		
		Optional<Account> account = accountRepository.findByUsername(username);

		if (!account.isPresent()) {
			Account newAccount = new Account(username, randomAlphabetic(8), "");
			accountRepository.save(newAccount);
		}
		
		super.successfulAuthentication(request, response, chain, authResult);
	}

}
