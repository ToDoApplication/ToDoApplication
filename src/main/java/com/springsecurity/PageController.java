package com.springsecurity;

import java.security.Principal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.springsecurity.dao.Account;
import com.springsecurity.dao.Grouped;
import com.springsecurity.dao.Task;
import com.springsecurity.model.AccountRepository;
import com.springsecurity.model.GroupedRepository;
import com.springsecurity.model.TaskRepository;
import com.springsecurity.model.ToDoListRepository;

/**
 * Kontroller odpowiedzialny za przekierowania oraz obsługujący
 * serwowanie widoków z foleru template przy pomocy pluginu thymeleaf
 * 
 * @author karolczukm
 */
@Controller
public class PageController {
	
	private final AccountRepository accountRepository;
	
	private final GroupedRepository groupedRepository;
	
	private final TaskRepository taskRepository;
	
	private final ToDoListRepository toDoListRepository;
	
	
	@Autowired
	public PageController(AccountRepository accountRepository, GroupedRepository groupedRepository, TaskRepository taskRepository, ToDoListRepository toDoListRepository) {
		this.accountRepository = accountRepository;
		this.groupedRepository = groupedRepository;
		this.taskRepository = taskRepository;
		this.toDoListRepository = toDoListRepository;
	}
	
	
    /**
     * <p>
     * Przekierowanie każdego przychodzącego pustego adresu na 
     * widoku index,html tak jak jest to domyślnie w ustawieniach.
     * </p>
     * @param Model model Model do przechowywania atrybutów
     * @return String Mapowanie na widok w folderze
     */
	@RequestMapping("/")
    public String base(Model model) {
        return "index";
    }
    
    /**
     * <p>
     * Obsługa mapowania wyświetlania głównej formatki wraz z wyświetlaną listą
     * istniejących zadań dla użytkownika.
     * </p>
     * @param Model model Model do przechowywania atrybutów
     * @return String Mapowanie na widok w folderze
     */
    @RequestMapping("/main")
    public String mainPage(Model model) {
        return "main";
    }
    
    /**
     * <p>
     * Obsługa mapowania dodawania nowego zadania.
     * </p>
     * @param Model model Model do przechowywania atrybutów
     * @param Principal principal Pryncypia zalogowanego użytkownika
     * @param Long id Atrybut opcjonalny identyfikatora zadania
     * @return ModelAndView Obiekt dostarczający widok oraz atrybuty
     */
    @RequestMapping("/addtask")
    public ModelAndView addTask(Principal principal, Model model, @RequestParam(name = "id",required = false) Long id) {
    	ModelAndView mav = new ModelAndView("addtask");
    	Optional<Account> account = accountRepository.findByUsername(principal.getName());
    	
    	if(!account.isPresent()) {
    		return new ModelAndView("/");
    	}
    	
    	Grouped grouped = new Grouped(account.get(), "", null);
		mav.addObject("group", grouped);
		mav.addObject("task", new Task(grouped, "", "", "", true));
    	
    	if (id!=null) {
    		Task task = taskRepository.findOne(id);
    		
    		if (task!=null) {
    			Grouped group = task.getGrouped();
    			mav.addObject("group", group);
    			mav.addObject("task", task);
    		}
    	}
    	
        return mav;
    }
    
    /**
     * <p>
     * Obsługa mapowania dodawania grup na widok.
     * </p>
     * @param Model model Model do przechowywania atrybutów
     * @param Principal principal Pryncypia zalogowanego użytkownika
     * @param Long id Atrybut opcjonalny identyfikatora grupy
     * @return ModelAndView Obiekt dostarczający widok oraz atrybuty
     */
    @RequestMapping("/addgroup")
    public ModelAndView addGroup(Principal principal, Model model, @RequestParam(name = "id",required = false) Long id) {
    	ModelAndView mav = new ModelAndView("addgroup");
    	
    	Optional<Account> account = accountRepository.findByUsername(principal.getName());
		if (!account.isPresent()) {
			return new ModelAndView("/");
		}
		
		mav.addObject("group", new Grouped(account.get(), "", null));
    	
		if (id!=null) {
			Grouped group = groupedRepository.findOne(id);
			if (group!=null)
				mav.addObject("group", group);
		}
    	
        return mav;
    }
    
    /**
     * <p>
     * Obsługa mapowania wyświetlania listy grup do widoku.
     * </p>
     * @param Model model Model do przechowywania atrybutów
     * @return String Mapowanie na widok w folderze
     */
    @RequestMapping("/listgroups")
    public String listGroups(Model model) {
        return "listgroups";
    }
}
