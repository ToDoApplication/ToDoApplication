package com.springsecurity;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.request.model.TaskModel;
import com.springsecurity.dao.Grouped;
import com.springsecurity.dao.Task;
import com.springsecurity.dao.ToDoList;
import com.springsecurity.model.GroupedRepository;
import com.springsecurity.model.TaskRepository;
import com.springsecurity.model.ToDoListRepository;
import com.utils.JWTUtils;

/**
 * Created by Marcin Jucha on 06.06.2017.
 */
@RestController
@RequestMapping("/api/task")
public class APITaskRestController {

    private final TaskRepository taskRepository;
    private final ToDoListRepository listRepository;
    private final GroupedRepository groupedRepository;

    @Autowired
    public APITaskRestController(TaskRepository taskRepository, GroupedRepository groupedRepository,
                                 ToDoListRepository listRepository) {
        this.taskRepository = taskRepository;
        this.groupedRepository = groupedRepository;
        this.listRepository = listRepository;
    }


    @RequestMapping(method = RequestMethod.POST)
    public void add(@RequestBody TaskModel model, HttpServletRequest req, HttpServletResponse res) {
        String username = JWTUtils.getAuthentication(req);

        if (username != null) {
            Grouped grouped = groupedRepository.getOne(model.getGroupId());
            Task task = model.getTask(grouped);
            taskRepository.save(task);
            groupedRepository.save(grouped);
            for (Map map: model.getList()) {
                ToDoList todo = model.getTodoList(task, map);
                listRepository.save(todo);
            }

            return;
        }

        res.setStatus(401);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void update(@RequestBody TaskModel model, HttpServletRequest req, HttpServletResponse res) {
        String username = JWTUtils.getAuthentication(req);

        if (username != null) {
            Task task = taskRepository.findOne(model.getId());
            model.updateTask(task);
            if (model.getList().isEmpty()) {
                for(Iterator<ToDoList> todoIterator = task.getList().iterator();
                    todoIterator.hasNext(); ) {
                    ToDoList todo = todoIterator .next();
                    todo.setTask(null);
                    todoIterator.remove();
                }
            }
            taskRepository.save(task);


            for (Map map : model.getList()) {
                Long id = model.getToDoId(map);
                ToDoList todo;
                if (id == null) {
                    todo = model.getTodoList(task, map);
                } else {
                    todo = listRepository.findOne(id);
                    model.updateTodo(todo, map);
                }
                listRepository.save(todo);
            }

            return;
        }

        res.setStatus(401);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody TaskModel model, HttpServletRequest req, HttpServletResponse res) {
        String username = JWTUtils.getAuthentication(req);

        if (username != null) {

            taskRepository.delete(model.getId());

            return;
        }

        res.setStatus(401);
    }
}