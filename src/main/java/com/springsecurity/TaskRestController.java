package com.springsecurity;

import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.request.model.TaskMappingHelper;
import com.springsecurity.dao.Account;
import com.springsecurity.dao.Grouped;
import com.springsecurity.dao.Task;
import com.springsecurity.dao.ToDoList;
import com.springsecurity.model.AccountRepository;
import com.springsecurity.model.GroupedRepository;
import com.springsecurity.model.TaskRepository;
import com.springsecurity.model.ToDoListRepository;


/**
 * <p>
 * Klasa kontrolera RESTowego dla operacji na zadaniach.
 * Klasa automaytcznie zarejestrowana jako Bean dzięki adnotacji kontrolera. Dodaje główną ścieżkę do
 * mapowania, a rozróżnia operacje zgodnie z przyjętą koncepcją REST, rozróżniając operacje nagłówkami.
 * </p>
 * 
 * <p>
 * Dodatkowo w związku z silnym powiązaniem listy kroków z zadaniem, większość operacji jest robiona w połączeniu z zadaniami,
 * natomiast nie przewidzieliśmy do tego zadania oddzielnego kontrolera.
 * </p>
 * 
 * @author karolczukm
 */
@RestController
@RequestMapping(TaskRestController.REQUEST_NAME)
public class TaskRestController {

	static final String REQUEST_NAME = "/task";
	
	private final TaskRepository taskRepository;
	
	private final AccountRepository accountRepository;
	
	private final GroupedRepository groupedRepository;
	
	private final ToDoListRepository toDoListRepository;
	
	/**
	 * <p>
	 * Kontroler dostarczający Beany {@link GroupedRepository}, {@link AccountRepository}, {@link TaskRepository} oraz 
	 * {@link ToDoListRepository}. Ustalający powiązanie z kontrolera repozytoriów na całą długość życia kontrolera.
	 * </p>
	 * 
	 * @param taskRepository Repozytorium zadań
	 * @param accountRepository Repozytorium kont
	 * @param groupedRepository Repozytorium grup
	 * @param toDoListRepository Repozytorium listy kroków zadania
	 */
	@Autowired
	TaskRestController(TaskRepository taskRepository, AccountRepository accountRepository, GroupedRepository groupedRepository, ToDoListRepository toDoListRepository) {
		this.taskRepository = taskRepository;
		this.accountRepository = accountRepository;
		this.groupedRepository = groupedRepository;
		this.toDoListRepository = toDoListRepository;
	}
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za pobieranie wszystkich zadań dostępnych dla zautoryzowanego użytkownika.
	 * Jeżeli nie potrafi znaleźć powiązanego konta zalogowanego użytkownika to zwraca wartość pustą.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @return Lista zgromadzonych w pomocniczą klasę zadań {@link Task} w grupy {@link Grouped}.
	 * @see TaskMappingHelper
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<TaskMappingHelper> showTasks(Principal principal) {
		Optional<Account> optional = this.accountRepository.findByUsername(principal.getName());
		List<TaskMappingHelper> tasks = new ArrayList<TaskMappingHelper>();
		Set<Grouped> groups = optional.get().getGroupeds();
		
		if (!optional.isPresent()) {
			return null;
		}
		
		for (Grouped group : groups) {
			TaskMappingHelper tmp = new TaskMappingHelper(group.getName(), group.getColour(), group.getTasks());
			tasks.add(tmp);
		}
		
		return tasks;
	}
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za pobieranie konkretnego zadania po dodaniu do ścieżki parametru identyfikatora zadania.
	 * Odpowiada ścieżce z parametrem całkowitoliczbowym oraz żądaniu typu GET.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param taskId Identyfikator szukanego zadania typu całkowitoliczbowego {@link Long}
	 * @return {@link Task} Instancję zadania lub wartość pustą, gdy nie ma użytkownika w bazie
	 */
	@RequestMapping(method = RequestMethod.GET, value="/{taskId}")
	public Task showTask(Principal principal, @PathVariable Long taskId) {
		Optional<Account> account = accountRepository.findByUsername(principal.getName());
		if (!account.isPresent()) {
			return null;
		}
		return taskRepository.findOne(taskId);
	}
	
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za zapis zadania na podstawie ciała żądania zawartego w żądaniu typu POST.
	 * Jeżeli nie może przetworzyć ciała żądania to zwraca status 400 z odpowiedzią o źle sformatownym 
	 * ciele JSON żądania.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param grouped {@link Grouped} Ciało żądania przetworzona przez Jackson do konkretnej instancji.
	 * @return Jeden z dwóch statusów, że udało się dodać zadanie lub brak zawartości.
	 */
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> addTask(Principal principal, @RequestBody Grouped grouped) {
		
		return this.accountRepository
			.findByUsername(principal.getName())
			.map(account -> {
				List<Task> tasks = grouped.getTasks();
				Task task = tasks.get(0);
				Grouped group = groupedRepository.findOne(grouped.getId());
				
				if (group==null) {
					return ResponseEntity.status(401).build();
				}
				
				Task newTask = new Task(group, task.getName(), task.getDescription(), task.getText(), task.getIsFavored());
				
				taskRepository.save(newTask);
				for (ToDoList list : task.getList()) {
					ToDoList newToDoList = new ToDoList(newTask, list.getText(), list.getIsCompleted());
					toDoListRepository.save(newToDoList);
				}
				
				
				URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path(TaskRestController.REQUEST_NAME + "/{taskId}")
					.buildAndExpand(newTask.getId()).toUri();
				
				return ResponseEntity.created(location).build();
			})
			.orElse(ResponseEntity.noContent().build());
	}

	/**
	 * <p>
	 * Metoda odpowiedzialna za aktualizację zadania, na podstawie przesłanej instancji zadania z ciała żądania.
	 * Jeżeli nie może przetworzyć ciała żądania to zwraca status 400 z odpowiedzią o źle sformatownym 
	 * ciele JSON żądania.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param grouped {@link Grouped} Ciało żądania przetworzona przez Jackson do konkretnej instancji.
	 * @return Jeden z dwóch statusów, że udało się aktualizować zadanie lub brak zawartości.
	 */
	@RequestMapping(method = RequestMethod.PUT)
	ResponseEntity<?> updateTask(Principal principal, @RequestBody Grouped grouped) {
		
		return this.accountRepository
			.findByUsername(principal.getName())
			.map(account -> {
				Task task = grouped.getTasks().get(0);
				Task updateTask = taskRepository.findOne(task.getId());
				
				
				updateTask.setName(task.getName());
				updateTask.setText(task.getText());
				updateTask.setDescription(task.getDescription());
				updateTask.setFavored(task.getIsFavored());
				
				for (ToDoList requestList : task.getList()) {
					
					if (requestList.getId()!=null) {

						ToDoList toDoList = toDoListRepository.getOne(requestList.getId());
						
						toDoList.setCompleted(requestList.getIsCompleted());
						toDoList.setText(requestList.getText());
						toDoList.setTask(updateTask);
						toDoList.setId(requestList.getId());
						toDoListRepository.save(toDoList);
						
					} else {
						
						ToDoList toDoList = new ToDoList(updateTask, requestList.getText(), requestList.getIsCompleted());
						toDoListRepository.save(toDoList);
					}
				}
				
				taskRepository.save(updateTask);
				
				URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path(TaskRestController.REQUEST_NAME + "/{taskId}")
					.buildAndExpand(updateTask.getId()).toUri();
				
				return ResponseEntity.created(location).build();
			})
			.orElse(ResponseEntity.noContent().build());
	}
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za kasowanie rekordu z encji zadania.
	 * Usuwa rekord na podstawie parametru podanego w ścieżce, który odpowiadać ma 
	 * wartości typu całowitego {@link Long}.
	 * </p>
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param taskId Identyfikator szukanego zadania typu całkowitoliczbowego {@link Long}
	 * @return Jeden z dwóch statusów, że udało się usunąć zadanie lub brak zawartości.
	 */
	@RequestMapping(method = RequestMethod.DELETE, value="/{taskId}")
	ResponseEntity<?> deleteTask(Principal principal, @PathVariable Long taskId) {
		
		return this.accountRepository
			.findByUsername(principal.getName())
			.map(account -> {
				taskRepository.delete(taskId);
				URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path(TaskRestController.REQUEST_NAME)
					.buildAndExpand().toUri();
				
				return ResponseEntity.created(location).build();
			})
			.orElse(ResponseEntity.noContent().build());
	}
	
}
