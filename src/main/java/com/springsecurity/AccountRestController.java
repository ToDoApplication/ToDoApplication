package com.springsecurity;

import java.security.Principal;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springsecurity.dao.Account;
import com.springsecurity.dao.Grouped;
import com.springsecurity.model.AccountRepository;
import com.springsecurity.model.GroupedRepository;

/**
 * <p>
 * Klasa kontrolera RESTowego dla operacji na kontach użytkowników.
 * Klasa automatycznie zarejestrowana jako Bean dzięki adnotacji kontrolera. Dodaje główną ścieżkę do
 * mapowania, a rozróżnia operacje zgodnie z przyjętą koncepcją REST, rozróżniając operacje nagłówkami.
 * </p>
 * 
 * @author karolczukm
 */
@RestController
@RequestMapping(AccountRestController.REQUEST_NAME)
public class AccountRestController {

	static final String REQUEST_NAME = "/account";
	
	private final AccountRepository accountRepository;
	
	/**
	 * <p>
	 * Kontroler dostarczający Bean {@link AccountRepository}, z adnotacją, która zajmuje się 
	 * wstrzyknięciem zależności do kontrolera.
	 * </p>
	 * 
	 * @param accountRepository Repozytorium kont
	 */
	@Autowired
	AccountRestController(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	/**
	 * <p>
	 * Funkcja pomocnicza do stwierdzenia czy użytkownik został zautoryzowany poprawnie.
	 * </p>
	 * @param principal Pryncypia dotyczące użytkownika systemu
	 * @return Pryncypia dotyczące użytkownika systemu
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Principal user(Principal principal) {
		return principal;
	}
	
	
	/**
	 * <p>
	 * Metoda odpowiedzialna za pobieranie szczegółów konkretnego konta po dodaniu do ścieżki parametru 
	 * identyfikatora odpowiedniego użytkownika.
	 * Odpowiada ścieżce z parametrem całkowitoliczbowym oraz żądaniu typu GET.
	 * </p>
	 * 
	 * @param principal Pryncypia jednostki zalogowanej {@link Principal}
	 * @param accountId Identyfikator szukanego użytkownika {@link Long}
	 * @return Konkretną instanjcę użytkownika w przypadku, gdy istnieje, lub null, gdy nie istnieje
	 */
	@RequestMapping(method = RequestMethod.GET, value="/{accountId}")
	public Account showUser(Principal principal, @PathVariable Long accountId) {
		return accountRepository.findOne(accountId);
	}
}
