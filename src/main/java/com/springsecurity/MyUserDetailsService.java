package com.springsecurity;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springsecurity.dao.Account;
import com.springsecurity.model.AccountRepository;

@Service
@Deprecated
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private AccountRepository accountRepository;
	
	public MyUserDetailsService() {
		super();
	}
	
	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		final Optional<Account> user = accountRepository.findByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException(username);
		}

		return new org.springframework.security.core.userdetails.User(username, user.get().getPassword(), true, 
			true, true, true, Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
	}

}
