package com.springsecurity;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * Klasa główna odpowiedzialna za start aplikacji webowej. Możliwe poprzez dodanie adnotacji do 
 * klasy @SpringBootApplication. Dodanie tej adnotacji to jest to samo co dodanie do klasy adnotacji
 * @Configuration, @EnableAutoConfiguration oraz @ComponentScan z ich domyślnymi atrybutami.
 * </p>
 * <p>
 * Dodaje w ten sposób kilka podstawowych konfigruacji (np. logowanie na linię poleceń, shader itp.)
 * oraz osadza gotowe biblioteki po spakowaniu w kontenerze serwera Tomcat. Odciąża również użytkownika od konfiguracji, 
 * dzięki zależnościom zdefiniowanym w pliku pom.xml.
 * </p>
 * <p>
 * Dodatkowo ustawia, gdzie aplikacja ma szukać paczek dla JPA. Adnotacje @EntityScan oraz @EnableJpaRepositories wskazują miejsca
 * z dao oraz modelami, a także mówią plugionwi spring-boot, że JPA jest domyślnym zarządcą integracji z bazą danych i 
 * tworzy konfigurację odpowiedzialną za mapowania oraz interfejs do bazy danych.
 * <p>
 * @author karolczukm
 */
@SpringBootApplication
@EnableJpaRepositories("com.springsecurity.model")
@EntityScan("com.springsecurity.dao")
public class SocialApplication  {

	/**
	 * <p>
	 * Punkt wejścia pomiędzy serwerem aplikacji, a serwerem zasobów,
	 * </p>
	 * @param Principal principal Pryncypia jednostki zautoryzowanej
	 * @return Mapa atrybutów do przekazania zwrotnie
	 */
	@RequestMapping("/me")
	public Map<String, String> user(Principal principal) {
		Map<String, String> map = new LinkedHashMap<>();
		map.put("name", principal.getName());
		return map;
	}
	
	/**
	 * <p>
	 * Funkcja główna odpowiedzialna za start aplikacji.
	 * </p>
	 * @param args Parametry linii poleceń
	 */
	public static void main(String[] args) {
		SpringApplication.run(SocialApplication.class, args);
	}


	/**
	 * <p>
	 * Serwer konfiguracji, który służy wystawieniu serwera zasobów, a mianowicie do połączenia się z API dla klientów,
	 * którzy łączą się po porcie 9000 i dzięki szczegółom ustawionym w pliku application.yaml.
	 * </p>
	 * @author karolczukm
	 */
	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
		@Override
		public void configure(HttpSecurity http) throws Exception {
			http
				.antMatcher("/me")
				.authorizeRequests().anyRequest().authenticated();
	  	}
	}
}
