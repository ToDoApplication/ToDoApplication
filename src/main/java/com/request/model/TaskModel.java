package com.request.model;

import com.springsecurity.dao.Grouped;
import com.springsecurity.dao.Task;
import com.springsecurity.dao.ToDoList;

import java.util.List;
import java.util.Map;

/**
 * Created by Marcin Jucha on 06.06.2017.
 */
public class TaskModel {
    private Integer groupId;

    private Integer id;
    private String name;
    private String text;
    private Boolean favored;
    private List<Map<String, Object>> list;

    public TaskModel() {}

    public TaskModel(Integer groupId, Integer id, String name, String text,
                     Boolean favored, List<Map<String, Object>> list) {
        this.groupId = groupId;
        this.id = id;
        this.name = name;
        this.text = text;
        this.favored = favored;
        this.list = list;
    }

    public Long getGroupId() {
        return Long.valueOf(groupId);
    }

    public Long getId() {
        return Long.valueOf(id);
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public Boolean getFavored() {
        return favored;
    }

    public List<Map<String, Object>> getList() {
        return list;
    }

    public Task getTask(Grouped grouped) {
        text = text.isEmpty() ? null : text;
        Task task = new Task(grouped, name, "", text, favored);
        return task;
    }

    public ToDoList getTodoList(Task task, Map<String, Object> map) {
        String text = (String) map.get("text");
        boolean completed = (boolean) map.get("completed");
        return new ToDoList(task, text, completed);
    }

    public void updateTask(Task task) {
        task.setFavored(favored);
        task.setName(name);
        task.setText(text.isEmpty() ? null : text);
    }

    public Long getToDoId(Map<String, Object> map) {
        Object id = map.get("id");
        if (id != null) {
            return Long.valueOf((Integer) id);
        }
        return null;
    }

    public void updateTodo(ToDoList todo, Map<String, Object> map) {
        String text = (String) map.get("text");
        boolean completed = (boolean) map.get("completed");
        todo.setText(text);
        todo.setCompleted(completed);
    }

}
