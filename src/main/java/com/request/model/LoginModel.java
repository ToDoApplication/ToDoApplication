package com.request.model;

import javax.validation.constraints.NotNull;

/**
 * Created by Marcin Jucha on 06.06.2017.
 */
public class LoginModel {
    private String username;

    @NotNull
    private String token;

    public LoginModel() {}

    public LoginModel(String username, String token) {
        this.username = username;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }
}
