package com.request.model;

import com.springsecurity.dao.Colour;

/**
 * Created by Marcin Jucha on 05.06.2017.
 */
public class GroupedModel {
    private Long id;
    private String name;
    private String colour;

    public GroupedModel() {}

    public GroupedModel(Long id, String name, String colour) {
        this.id = id;
        this.name = name;
        this.colour = colour;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Colour getColour() {
        switch (colour) {
            case "RED":
                return Colour.RED;
            case "ORANGE":
                return Colour.ORANGE;
            case "YELLOW":
                return Colour.YELLOW;
            case "GREEN":
                return Colour.GREEN;
            case "TEAL_BLUE":
                return Colour.TEAL_BLUE;
            case "BLUE":
                return Colour.BLUE;
            case "PURPLE":
                return Colour.PURPLE;
            case "PINK":
                return Colour.PINK;
            default:
                return Colour.RED;
        }
    }
}
