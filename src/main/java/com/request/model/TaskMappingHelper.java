package com.request.model;

import java.util.List;

import com.springsecurity.TaskRestController;
import com.springsecurity.dao.Colour;
import com.springsecurity.dao.Task;

/**
 * <p>
 * Klasa wspierająca przetwarzanie oraz mapowanie wyników w kontrolerze zadań.
 * Używana tylko przy pobieraniu bardziej skomplikowanych struktur z JPA.
 * </p>
 * 
 * @see TaskRestController
 * @author karolczukm
 */
public class TaskMappingHelper {
	
	private String name;
		
	private Colour colour;
	
	private List<Task> tasks;
	
	TaskMappingHelper() {};
	
	public TaskMappingHelper(String name, Colour colour, List<Task> tasks) {
		this.name = name;
		this.colour = colour;
		this.tasks = tasks;
	}

	public String getName() {
		return name;
	}

	public Colour getColour() {
		return colour;
	}

	public List<Task> getTasks() {
		return tasks;
	}
	
}
