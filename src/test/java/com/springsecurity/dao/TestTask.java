package com.springsecurity.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestTask {

	private Task zadanie;
	
	@Before
	public void dodajzadanie() {
	zadanie = new Task(null, "pobudka", "poranne wstawanie", "musisz rano wstac zeby zrobic co musisz", true);
	}
	
	@Test
	public void testname() {
	Assert.assertEquals("zla nazwa", "pobudka", zadanie.getName());
	}
	
	@Test
	public void testnotname() {
	Assert.assertNotEquals("zla nazwa", "nie pobudka", zadanie.getName());
	}
	
	@Test
	public void testdesript() {
	Assert.assertEquals("zly tekst", "poranne wstawanie", zadanie.getDescription());
	}
	
	@Test
	public void testnotdesript() {
	Assert.assertNotEquals("zly tekst", "pobudka", zadanie.getDescription());
	}
	
	@Test
	public void testtext() {
	Assert.assertEquals("zly opis", "musisz rano wstac zeby zrobic co musisz", zadanie.getText());
	}
	
	@Test
	public void testnottext() {
	Assert.assertNotEquals("zly opis", "pobudka", zadanie.getText());
	}
	
	@Test
	public void testfavoured() {
	Assert.assertTrue("sprawdzanie czy ulubiony działa", zadanie.getIsFavored());
	}

}
