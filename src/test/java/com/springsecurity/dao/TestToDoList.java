package com.springsecurity.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestToDoList {

private ToDoList lista;
	
	@Before
	public void dodajliste() {
	lista = new ToDoList(null, "zadaniacodzienne", true);
	}
	
	@Test
	public void testtekst_lista() {
	Assert.assertEquals("zly tekst", "zadaniacodzienne", lista.getText());
	}
	
	@Test
	public void testnottekst_lista() {
	Assert.assertNotEquals("zly tekst", "zadanianiecodzienne", lista.getText());
	}

	@Test
	public void test_completed() {
	Assert.assertTrue("musi byc true, jeśli jest i rzuca bład to coś nie tak ", lista.getIsCompleted());
	}
	
}
