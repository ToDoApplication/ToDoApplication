package com.springsecurity.dao;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.springsecurity.dao.Account;


public class TestAccount {

	private Account konto;
	@Before
	public void dodajusera() {
	konto = new Account("adrian","pass123");
	}
	
	
	@Test
	public void testusername() {
	Assert.assertEquals("nazwa zle pobrana", "adrian", konto.getUsername());
	}
	
	@Test
	public void testpassword() {
	Assert.assertEquals("haslo zle pobrane", "pass123", konto.getPassword());
	}
	
	
	
}
